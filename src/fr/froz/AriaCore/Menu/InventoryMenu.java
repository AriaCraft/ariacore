package fr.froz.AriaCore.Menu;



import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class InventoryMenu implements Listener {
    Inventory inv;
    ArrayList buttonTypeList = new ArrayList();
    ArrayList buttonList = new ArrayList();
    ArrayList buttonItem = new ArrayList();
    public InventoryMenu(String name, int slot){
        inv = Bukkit.createInventory(null,slot,name);
    }
    public void addButton(String name, String server, Material m,int slot){
        ItemStack i = new ItemStack(m,1);
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(name);
        i.setItemMeta(meta);
        inv.setItem(slot,i);
        inv.setItem(slot,i);
        buttonList.add(server);
        buttonTypeList.add(ButtonType.BUNGEESEND);
        buttonItem.add(i);
    }
    public void addButton(String name, Location loc, Material m,int slot){
        ItemStack i = new ItemStack(m,1);
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(name);
        i.setItemMeta(meta);
        inv.setItem(slot,i);
        buttonList.add(loc);
        buttonTypeList.add(ButtonType.TELEPORT);
        buttonItem.add(i);
    }
    public void addButton(String name, Inventory inv, Material m,int slot){
        ItemStack i = new ItemStack(m,1);
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(name);
        i.setItemMeta(meta);
        inv.setItem(slot,i);
        buttonList.add(inv);
        buttonTypeList.add(ButtonType.OTHERMENU);
        buttonItem.add(i);
    }
    public Inventory getInventory(){
        return inv;
    }
}
