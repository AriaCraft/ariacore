package fr.froz.AriaCore;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;

@SuppressWarnings("unused")
public class AriaAPI {
    public static void getConfig(String name) throws Exception {
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new StringReader("config%"+name));
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');
        modifiedSentence = inFromServer.readLine();
        String[] lines = modifiedSentence.split("%");
        clientSocket.close();
        if(!Files.exists(Paths.get("plugins/"+name))){
            Files.createDirectories(Paths.get("plugins/"+name));
        }
        Files.deleteIfExists(Paths.get("plugins/"+name+"/config.yml"));
        PrintWriter pw = new PrintWriter(new FileWriter("plugins/"+name+"/config.yml"));
        int i = 0;
        while(i<lines.length){
            pw.write(lines[i]+"\n");
            i++;
        }
        pw.close();
    }
    public static int getMoney(Player p,String moneyType)throws Exception{
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new StringReader("money%"+p.getName().toString()+"%get%"+moneyType));
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');
        modifiedSentence = inFromServer.readLine();
        clientSocket.close();
        return Integer.valueOf(modifiedSentence);
    }
    public static int addMoney(Player p,String moneyType,int n)throws Exception{
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new StringReader("money%"+p.getName().toString()+"%add%"+moneyType+"%"+String.valueOf(n)));
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');
        modifiedSentence = inFromServer.readLine();
        clientSocket.close();
        return Integer.valueOf(modifiedSentence);
    }
    public static void setGrade(Player p,String grade)throws Exception{
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new StringReader("grade%"+p.getName().toString()+"%"+grade));
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');
        clientSocket.close();
    }
    public static void sendTo(Player p, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        p.sendPluginMessage(Bukkit.getPluginManager().getPlugin("AriaCore"), "BungeeCord", out.toByteArray());
    }
    public static void lobby(Player p){
        String[] lobby = {"lobby1","lobby2","lobby3"};
        int max = 0, min = 2;
        int to = (int) (Math.random() * (max - min)) + min;
        sendTo(p,lobby[to]);
    }
    public static void addPermissions(UUID ui, ArrayList<String> toAdd, ArrayList<String> toRemove) {
        PermissionAttachment a = Bukkit.getPlayer(ui).addAttachment(Bukkit.getPluginManager().getPlugin("AriaCore"));
        int i = 0;
        while (i < toAdd.size()) {
            a.setPermission(toAdd.get(i), true);
            i++;
        }
        i = 0;
        while (i < toRemove.size()) {
            a.setPermission(toRemove.get(i), false);
            i++;
        }
    }

}
